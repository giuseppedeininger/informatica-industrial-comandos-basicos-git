Parque Eólico Digital é um projeto que visa oferecer uma solução digital de gestão, manutenção e acompanhamento de desempenho de parques eólicos. 

O objetivo do projeto é oferecer uma solução eficiente na produção de energia eólica ao mesmo tempo que diminui os custos, melhorando, assim, o retorno financeiro do parque eólico.   

Cada parque eólico possui características únicas de acordo com as condições meteorológicas do local instalado. Hoje em dia, existem opções limitadas de turbinas e configurações de parques eólicos. A eficiência dos parques eólicos encontra-se limitada pela falta de alternativas que abranjam todas as possíveis condições meteorológicas e suas mudanças ao longo dos anos. Além disso, são empregadas vistorias preventivas manuais que demandam ou grande quantidade de mão-de-obra ou grande quantidade de tempo, o que resulta em uma manutenção tardia e influencia a produtividade do parque.

O projeto tem como pré-requisito um projeto de parque eólico já instalado para poder operar. Inicialmente, este projeto não tem como finalidade auxiliar na execução de projetos e sim na sua gestão, manutenção e futura modificação.  Adicionalmente, trata-se de um software que aplica conceitos de time learning e machine learning, dessa forma, o software se tornara mais robusto e confiável à medida que novos dados serão analisados, necessitando de um tempo para oferecer resultados apurados.

O mercado alvo deste produto são empresas detentoras de parques eólicos, produtores de energia eólica. 
