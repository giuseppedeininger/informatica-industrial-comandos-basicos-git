package com.company;

import java.util.Scanner;

public class Main {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int[] myIntValues = new int[5];

        getIntegers(myIntValues);
        System.out.println("The initial sequence was ");
        printArray(myIntValues);
        sortIntegers(myIntValues);
        System.out.println("The sorted sequence is ");
        printArray(myIntValues);



    }

    static public void getIntegers (int[] values)
    {
        System.out.println("Enter " + values.length + " numbers");

        for (int i = 0 ; i < values.length ; i++)
            values[i] = scanner.nextInt();
    }

    static public void printArray (int[] array)
    {

        for (int i = 0 ; i < array.length ; i++)
            System.out.print(array[i] + " ");

        System.out.println(" ");
    }

    static public void sortIntegers (int[] array)
    {
        int aux = 0;

        for (int i = 0 ; i < array.length - 1 ; i++)
        {
            //System.out.println("i: " + i);
            for (int j = 0 ; j < array.length - 1 ; j++)
            {
                //System.out.println("j: " + j);
                if (array [j] < array [j+1])
                {
                    aux = array[j];
                    array[j] = array [j+1];
                    array[j+1] = aux;
                }
            }
        }
    }
}
